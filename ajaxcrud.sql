-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 04, 2021 at 11:06 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajaxcrud`
--

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE `usertable` (
  `id` int(9) NOT NULL,
  `customerId` varchar(100) NOT NULL,
  `companyName` text NOT NULL,
  `gstNos` varchar(100) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `contactNos` bigint(100) NOT NULL,
  `alertnateNos` bigint(100) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `series` varchar(255) NOT NULL,
  `modelNos` varchar(100) NOT NULL,
  `serialNos` varchar(100) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`id`, `customerId`, `companyName`, `gstNos`, `firstName`, `lastName`, `contactNos`, `alertnateNos`, `address1`, `address2`, `email`, `series`, `modelNos`, `serialNos`, `quantity`, `price`, `created_at`) VALUES
(4, 'Cust4567', 'demo company', 'gst12345', 'Mahadev', 'Ingawale', 9970410333, 9970410332, 'Shirwal 412801', 'Pune-123456', 'ingawalemr12@gmail.com', 'ABC12345', 'XYZ12345', '1234567', '1000', 4972.3, '2021-06-04 14:23:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usertable`
--
ALTER TABLE `usertable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usertable`
--
ALTER TABLE `usertable`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
