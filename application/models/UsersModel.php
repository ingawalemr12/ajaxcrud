<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

	public function create($formArray)
	{
		$this->db->insert('usertable', $formArray);
		$id = $this->db->insert_id(); // insert record with id
		return $id;
	}

	public function all()
	{
		$result = $this->db->order_by('id', 'ASC')->get('usertable')->result_array();
		return $result;
	}

	public function getRow($id) 
	{
		$this->db->where('id', $id);  // fetch record with id
		$row = $this->db->get('usertable')->row_array();
		return $row;
	}

	public function update($id,$formArray) 
	{
		$this->db->where('id', $id);  // fetch record with id
		$this->db->update('usertable', $formArray);
		return $id;
	}

	function delete($id)
	{
		$this->db->where('id', $id); 
		$this->db->delete('usertable');
	}
}