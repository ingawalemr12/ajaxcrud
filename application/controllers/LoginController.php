<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
	
	public function __construct(){ 
		parent::__construct();
		
		$this->load->library('form_validation');
	}

	public function login()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');

		if ($this->form_validation->run()==TRUE) 
		{	
			# validation success......
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if ($email=='ingawalemr12@gmail.com' && $password=='user@123') 
			{
				$this->session->set_userdata('user', $email);
				$this->session->set_flashdata('success','Login successful, Go to Home Page');
				//redirect(base_url().'CarModel/index');
				redirect(base_url().'UsersController/index');
			}  else
			{	
				echo "error";
			}
		} 
		else
		{	
			# validation errors 
			$this->load->view('login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('user');
		redirect(base_url().'LoginController/login');
	}
}
