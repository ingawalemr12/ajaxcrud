<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersController extends CI_Controller {

	public function __construct()
	{ 
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->model('UsersModel');
		$user =$this->session->userdata('user');
		if (empty($user)) {
			$this->session->set_flashdata('msg','Your session has been expired');
			redirect(base_url().'LoginController/login');
		}
	}

	public function index()
	{
		$this->load->model('UsersModel');
		$rows = $this->UsersModel->all();
		$data['rows'] = $rows;
		$this->load->view('list', $data);
	}

	public function showCreateform()
	{
		$html = $this->load->view('create.php','',true);
		$response['html'] = $html;
		echo json_encode($response);
	}

	public function saveModel()
	{
		$this->load->model('UsersModel');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('customerId', 'Customer Id', 'required|trim');
		$this->form_validation->set_rules('companyName', 'Company Name', 'required|trim');
		$this->form_validation->set_rules('contactNos', 'Contact Nos', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('series', 'series', 'required|trim');

		if($this->form_validation->run() == true) {
			# save entries in db table
			$formArray=array();
			$formArray['customerId'] = $this->input->post('customerId');
			$formArray['companyName'] = $this->input->post('companyName');
			$formArray['gstNos'] = $this->input->post('gstNos');
			$formArray['firstName'] = $this->input->post('firstName');
			$formArray['lastName'] = $this->input->post('lastName');
			$formArray['contactNos'] = $this->input->post('contactNos');
			$formArray['alertnateNos'] = $this->input->post('alertnateNos');
			$formArray['address1'] = $this->input->post('address1');
			$formArray['email'] = $this->input->post('email');
			$formArray['address2'] = $this->input->post('address2');
			
			$formArray['series'] = $this->input->post('series');
			$formArray['modelNos'] = $this->input->post('modelNos');
			$formArray['serialNos'] = $this->input->post('serialNos');
			$formArray['quantity'] = $this->input->post('quantity');
			$formArray['price'] = $this->input->post('price');
			

			$id = $this->UsersModel->create($formArray);  // insert record with id

			$row = $this->UsersModel->getRow($id); 		// fetch record with id
			$vdata['row'] = $row;
			$rowhtml = $this->load->view('rows_data', $vdata, true);
			$response['row'] = $rowhtml; 

			$response['status'] = 1;
			$response['message'] = "<div class='alert alert-success'>Record has been added successfully</div>";  
		}
		else
		{
			# show error message
			$response['status'] = 0;
			$response['customerId']= strip_tags(form_error('customerId'));
			$response['companyName']= strip_tags(form_error('companyName'));
			$response['contactNos']= strip_tags(form_error('contactNos'));
			$response['email']= strip_tags(form_error('email'));
			$response['series']= strip_tags(form_error('series'));
		}
		 echo json_encode($response);
	}
	
	public function edit($id)
	{
		$row = $this->UsersModel->getRow($id); 		// fetch record with id
		$data['row'] = $row;
		$html = $this->load->view('edit.php',$data,true);
		$response['html'] = $html;
		echo json_encode($response);
	}

	public function updateModel()
	{
		$this->load->model('UsersModel');
		$id = $this->input->post('id');
		$row = $this->UsersModel->getRow($id);
		if (empty($row)) {
			$response['msg'] = "Record not found or record deleted";
			$response['status'] = 100;
			echo json_encode($response); exit();
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('customerId', 'Customer Id', 'required|trim');
		$this->form_validation->set_rules('companyName', 'Company Name', 'required|trim');
		$this->form_validation->set_rules('contactNos', 'Contact Nos', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('series', 'series', 'required|trim');

		if($this->form_validation->run() == true) {
			# update entries in db table

			$formArray=array();
			$formArray['customerId'] = $this->input->post('customerId');
			$formArray['companyName'] = $this->input->post('companyName');
			$formArray['gstNos'] = $this->input->post('gstNos');
			$formArray['firstName'] = $this->input->post('firstName');
			$formArray['lastName'] = $this->input->post('lastName');
			$formArray['contactNos'] = $this->input->post('contactNos');
			$formArray['alertnateNos'] = $this->input->post('alertnateNos');
			$formArray['address1'] = $this->input->post('address1');
			$formArray['email'] = $this->input->post('email');
			$formArray['address2'] = $this->input->post('address2');
			
			$formArray['series'] = $this->input->post('series');
			$formArray['modelNos'] = $this->input->post('modelNos');
			$formArray['serialNos'] = $this->input->post('serialNos');
			$formArray['quantity'] = $this->input->post('quantity');
			$formArray['price'] = $this->input->post('price');

			$id = $this->UsersModel->update($id, $formArray);  // update record with id

			$row = $this->UsersModel->getRow($id); 		// fetch record with id
			$response['row'] = $row; 

			$response['status'] = 1;
			$response['message'] = "<div class='alert alert-success'>Record has been updated successfully</div>";  
		}
		else
		{
			# show error message
			$response['status'] = 0;
			$response['customerId']= strip_tags(form_error('customerId'));
			$response['companyName']= strip_tags(form_error('companyName'));
			$response['contactNos']= strip_tags(form_error('contactNos'));
			$response['email']= strip_tags(form_error('email'));
			$response['series']= strip_tags(form_error('series'));
		}
		 echo json_encode($response);
	}

	function delete($id)
	{
		$this->load->model('UsersModel');
		$row = $this->UsersModel->getRow($id);

		if (empty($row)) {
			$response['msg'] = "<div class='alert alert-warning'>Record already deleted!</div>";
			$response['status'] = 0;
			echo json_encode($response); 
		}
		else
		{
			$this->UsersModel->delete($id);
			$response['msg'] = "<div class='alert alert-success'>Record has been deleted successfully</div>"; 
			$response['status'] = 1;
			echo json_encode($response); 
		}
	}
}
