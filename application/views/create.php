
<form action="" method="post" name="createCarModel" id="createCarModel">
	<div class="modal-body">
		<h5>Customer Details</h5>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Customer Id</label>
			 		<input type="text" name="customerId" id="customerId" value="" class="form-control">
			 		<p class="customerIdError"></p>
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Company Name</label>
			 		<input type="text" name="companyName" id="companyName" value="" class="form-control">
			 		<p class="companyNameError"></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>GST Nos</label>
			 		<input type="text" name="gstNos" id="gstNos" value="" class="form-control" >
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>First Name</label>
			 		<input type="text" name="firstName" id="firstName" value="" class="form-control" >
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Last Name</label>
			 		<input type="text" name="lastName" id="lastName" value="" class="form-control" >
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Contact Nos</label>
			 		<input type="text" name="contactNos" id="contactNos" value="" class="form-control" >
			 		<p class="contactNosError"></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Alertnate Nos</label>
			 		<input type="text" name="alertnateNos" id="alertnateNos" value="" class="form-control">
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Address1</label>
			 		<textarea name="address1" id="address1" value="" class="form-control" ></textarea>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Email</label>
			 		<input type="email" name="email" id="email" value="" class="form-control">
			 		<p class="emailError"></p>
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Address2</label>
			 		<textarea name="address2" id="address2" value="" class="form-control" ></textarea>
				</div>
			</div>
		</div> <hr>	<br><hr>

		<h5>Product Details</h5>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Series</label>
			 		<input type="text" name="series" id="series" value="" class="form-control">
			 		<p class="seriesError"></p>
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Model Nos</label>
			 		<input type="text" name="modelNos" id="modelNos" value="" class="form-control">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Serial Nos</label>
			 		<input type="text" name="serialNos" id="serialNos" value="" class="form-control">
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Quantity</label>
			 		<input type="text" name="quantity" id="quantity" value="" class="form-control">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
			 		<label>Price</label>
			 		<input type="text" name="price" id="price" value="" class="form-control">
			 	</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<a href="#" class="btn btn-primary">+add</a>
			 	</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
	    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	    <button type="submit" class="btn btn-primary">Save </button>
	</div>
</form>      