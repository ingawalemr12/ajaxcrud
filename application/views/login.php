<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AJAX </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
</head>

<body>

<div class="container p-4">
	<div class="row">
		<div class="col-md-12" style="margin-left: 5%;">
			<div class="bg-light text-center">    
		        <h2>Login Here</h2>
		    </div>
		</div>
		
		<div class="col-md-8" style="margin-left: 15%;">
			<form method="post" action="<?php //echo base_url('LoginController/login'); ?>" >
			    <div class="form-group">
			 		<label>Email</label>
			 		<input type="email" name="email" id="email" value="<?php echo set_value('email') ?>"  class="form-control <?php echo (form_error('email') !="") ? 'is-invalid':'';?>">
			 		<?php echo form_error('email'); ?>
				</div>
			    
	            <div class="form-group">
	                <label for="pwd">Password</label>
	                <input type="password" class="form-control <?php echo (form_error('password') !="") ? 'is-invalid':'';?>" name="password" id="password">
	                <?php echo form_error('password'); ?>
	            </div>
	            <div class="form-group form-check">
	                <label class="form-check-label">
	                <input class="form-check-input" type="checkbox" name="view_password" id="view_password" onclick="myFunction()">Show Password
	                </label>
	            </div>
	            
	            <button type="submit" class="btn btn-primary">Sign In</button> 
            </form>
        </div>
    	
		</div>
	</div>
</div>

    
    <form method="post" action="<?php //echo base_url('LoginController/login'); ?>" >
                
                
    </form>
</body>
</html>

<script>
function myFunction() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";    // show text of password
  } else {
    x.type = "password";    // show invisible text of password
  }
}
</script>



</body>
</html>
