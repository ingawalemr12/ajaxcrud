<!DOCTYPE html>
<html>
<head>
<title>AJAX </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row pt-3 ">
				<div class="col-md-6 text-right">
					<h3 class="heading">AJAX CRUD APPLICATION</h3>
				</div>
				<div class="col-md-6 text-right">
					<a href="<?php echo base_url('LoginController/logout'); ?>" class="btn btn-danger">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row pt-3">
			<div class="col-md-4">
				<h4 class="ml-3 text-center" style="background: #efebe7;">Customer & Product Details</h4>
			</div>
			<div class="col-md-7 text-right">
				<a href="javascript:void(0);" onclick="showModal()" class="btn btn-primary">Create</a>
			</div>

			<div class="col-md-12 pt-3">
				<table class="table table-bordered table-sm" id="carModelList">
					<tbody>
						<tr>
							<th>ID</th>
							<th>Customer Id</th>
							<th>Company Name</th>
							<th>GST Nos</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Contact Nos</th>
							<th>Alertnate Nos</th>
							<th>Address1</th>
							<th>Address2</th>
							<th>Email</th>
							
							<th>Series</th>
							<th>Model Nos</th>
							<th>Serial Nos</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>

						<?php 
						if (!empty($rows)) {
							foreach ($rows as $row) { 
								$data['row'] = $row; // shows fetch data is appended- AJAX
								$this->load->view('rows_data', $data);
							}
						} else {?>
							<tr>
								<td>Record Not found</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal to insert data in table -->
	<div class="modal fade" id="createCar" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="title">AJAX CRUD Function</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div id="response">
	      	
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal to show Alert Message -->
	<div class="modal fade" id="ajaxResponseModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Alert Message</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      
	      	<div class="modal-body">

	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
		        </button>
			</div>
	      
	    </div>
	  </div>
	</div>

	<!-- Modal to show Alert Message -->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      
	      	<div class="modal-body">

	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
		        </button>
		        <button type="button" class="btn btn-danger" onclick="deleteNow();">Yes!
		        </button>
			</div>
	      
	    </div>
	  </div>
	</div>

	<script type="text/javascript">

		function showModal(){
			// show modal only
			$("#createCar").modal("show");
			$("#createCar #title").html("Create Record");
			$.ajax({
				url: '<?php echo base_url().'UsersController/showCreateform' ?>',
				type: 'POST',
				data:{},
				dataType: 'json',
				success : function(response){
				  $("#response").html(response["html"]);
				}
			});
		}

		// Create Record
		$("body").on("submit", "#createCarModel", function(e){
			e.preventDefault();
			//alert();

			$.ajax({
				url: '<?php echo base_url('UsersController/saveModel') ?>',
				type: 'POST',
				data:$(this).serializeArray(),
				dataType: 'json',
				success: function(response){

					if (response['status'] == 0) {
						if (response["customerId"] !="") {
						  $(".customerIdError").html(response["customerId"]).addClass('invalid-feedback d-block');
						  $("#customerId").addClass('is-invalid');
						}
						else
						{
						  $(".customerIdError").html("").removeClass('invalid-feedback d-block');
						  $("#customerId").removeClass('is-invalid');
						}

						if (response["companyName"] !="") {
						  $(".companyNameError").html(response["companyName"]).addClass('invalid-feedback d-block');
						  $("#companyName").addClass('is-invalid');
						}
						else
						{
						  $(".companyNameError").html("").removeClass('invalid-feedback d-block');
						  $("#companyName").removeClass('is-invalid');
						}

						if (response["contactNos"] !="") {
						  $(".contactNosError").html(response["contactNos"]).addClass('invalid-feedback d-block');
						  $("#contactNos").addClass('is-invalid');
						}
						else
						{
						  $(".contactNosError").html("").removeClass('invalid-feedback d-block');
						  $("#contactNos").removeClass('is-invalid');
						}

						if (response["email"] !="") {
						  $(".emailError").html(response["email"]).addClass('invalid-feedback d-block');
						  $("#email").addClass('is-invalid');
						}
						else
						{
						  $(".emailError").html("").removeClass('invalid-feedback d-block');
						  $("#email").removeClass('is-invalid');
						}

						if (response["series"] !="") {
						  $(".seriesError").html(response["series"]).addClass('invalid-feedback d-block');
						  $("#series").addClass('is-invalid');
						}
						else
						{
						  $(".seriesError").html("").removeClass('invalid-feedback d-block');
						  $("#series").removeClass('is-invalid');
						}
					}
					else
					{
						//show alert message after data is inserted in db table
						$("#createCar").modal("hide");
						$("#ajaxResponseModal .modal-body").html(response["message"]);
						$("#ajaxResponseModal").modal("show");

						//show form validation success, text fields are selected
						$(".customerIdError").html("").removeClass('invalid-feedback d-block');
						$("#customerId").removeClass('is-invalid');

						$(".companyNameError").html("").removeClass('invalid-feedback d-block');
						$("#companyName").removeClass('is-invalid');

						$(".contactNosError").html("").removeClass('invalid-feedback d-block');
						$("#contactNos").removeClass('is-invalid');

						$(".emailError").html("").removeClass('invalid-feedback d-block');
						$("#email").removeClass('is-invalid');

						$(".seriesError").html("").removeClass('invalid-feedback d-block');
						$("#series").removeClass('is-invalid');
					}

					// shows appended/inserted data on page, without refresh the page
					// i.e. $row = $this->UsersModel->getRow($id);
					$("#carModelList").append(response["row"]);
					
				}
			});
		});

	
		//  show modal with its fetched record, as per id
		function showeditForm(id){
			//alert(id);
			$("#createCar .modal-title").html("Edit Form Record");
			$.ajax({
				url: '<?php echo base_url('UsersController/edit/') ?>'+id,
				type: 'POST',
				dataType: 'json',
				success: function(response){
					$("#createCar #response").html(response["html"]);
					$("#createCar").modal("show");
				}
			});
		}

			// Edit Record
		$("body").on("submit", "#editCarModel", function(e){
			e.preventDefault();
			//alert();

			$.ajax({
				url: '<?php echo base_url('UsersController/updateModel/') ?>',
				type: 'POST',
				data:$(this).serializeArray(),
				dataType: 'json',
				success: function(response){
					if (response['status'] == 0) {

						if (response["customerId"] !="") {
						  $(".customerIdError").html(response["customerId"]).addClass('invalid-feedback d-block');
						  $("#customerId").addClass('is-invalid');
						}
						else
						{
						  $(".customerIdError").html("").removeClass('invalid-feedback d-block');
						  $("#customerId").removeClass('is-invalid');
						}

						if (response["companyName"] !="") {
						  $(".companyNameError").html(response["companyName"]).addClass('invalid-feedback d-block');
						  $("#companyName").addClass('is-invalid');
						}
						else
						{
						  $(".companyNameError").html("").removeClass('invalid-feedback d-block');
						  $("#companyName").removeClass('is-invalid');
						}

						if (response["contactNos"] !="") {
						  $(".contactNosError").html(response["contactNos"]).addClass('invalid-feedback d-block');
						  $("#contactNos").addClass('is-invalid');
						}
						else
						{
						  $(".contactNosError").html("").removeClass('invalid-feedback d-block');
						  $("#contactNos").removeClass('is-invalid');
						}

						if (response["email"] !="") {
						  $(".emailError").html(response["email"]).addClass('invalid-feedback d-block');
						  $("#email").addClass('is-invalid');
						}
						else
						{
						  $(".emailError").html("").removeClass('invalid-feedback d-block');
						  $("#email").removeClass('is-invalid');
						}

						if (response["series"] !="") {
						  $(".seriesError").html(response["series"]).addClass('invalid-feedback d-block');
						  $("#series").addClass('is-invalid');
						}
						else
						{
						  $(".seriesError").html("").removeClass('invalid-feedback d-block');
						  $("#series").removeClass('is-invalid');
						}
					}
					else
					{
						//success
						$("#createCar").modal("hide");
						$("#ajaxResponseModal .modal-body").html(response["message"]);
						$("#ajaxResponseModal").modal("show");

						//show form validation success, text fields are selected
						$(".customerIdError").html("").removeClass('invalid-feedback d-block');
						$("#customerId").removeClass('is-invalid');

						$(".companyNameError").html("").removeClass('invalid-feedback d-block');
						$("#companyName").removeClass('is-invalid');

						$(".contactNosError").html("").removeClass('invalid-feedback d-block');
						$("#contactNos").removeClass('is-invalid');

						$(".emailError").html("").removeClass('invalid-feedback d-block');
						$("#email").removeClass('is-invalid');

						$(".seriesError").html("").removeClass('invalid-feedback d-block');
						$("#series").removeClass('is-invalid');

						//show updated record on page
						var id = response["row"]["id"];
						$("#row-"+id+" .modelCustomerId").html(response["row"]["customerId"]);
						$("#row-"+id+" .modelCompanyName").html(response["row"]["companyName"]);
						$("#row-"+id+" .modelgstNos").html(response["row"]["gstNos"]);
						$("#row-"+id+" .modelfirstName").html(response["row"]["firstName"]);
						$("#row-"+id+" .modellastName").html(response["row"]["lastName"]);

						$("#row-"+id+" .modelcontactNos").html(response["row"]["contactNos"]);
						$("#row-"+id+" .modelalertnateNos").html(response["row"]["alertnateNos"]);
						$("#row-"+id+" .modeladdress1").html(response["row"]["address1"]);
						$("#row-"+id+" .modeladdress2").html(response["row"]["address2"]);
						$("#row-"+id+" .modelemail").html(response["row"]["email"]);

						$("#row-"+id+" .modelseries").html(response["row"]["series"]);
						$("#row-"+id+" .modelmodelNos").html(response["row"]["modelNos"]);
						$("#row-"+id+" .modelserialNos").html(response["row"]["serialNos"]);
						$("#row-"+id+" .modelquantity").html(response["row"]["quantity"]);
						$("#row-"+id+" .modelprice").html(response["row"]["price"]);

					}
					
				}
			});
		});

		// modal shows confirm Delete Modal
		function confirmDeleteModel(id){
			//alert(id);
			$("#deleteModal").modal("show");
			$("#deleteModal .modal-body").html("Do you want delete the #"+id+" ?");

			$("#deleteModal").data("id", id);
		}

		function deleteNow()
		{
			var id = $("#deleteModal").data('id');

			$.ajax({
				url: '<?php echo base_url('UsersController/delete/') ?>'+id,
				type: 'POST',
				data:$(this).serializeArray(),
				dataType: 'json',
				success: function(response){
					if (response['status'] == 1) {
						$("#deleteModal").modal("hide");
						$("#ajaxResponseModal .modal-body").html(response["msg"]);
						$("#ajaxResponseModal").modal("show");
					}
					else
					{
						$("#deleteModal").modal("hide");
						$("#ajaxResponseModal .modal-body").html(response["msg"]);
						$("#ajaxResponseModal").modal("show");
					}
				}
			});
		}
	</script>
</body>
</html>